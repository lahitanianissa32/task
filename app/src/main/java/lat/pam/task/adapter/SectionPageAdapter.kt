package lat.pam.task.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import lat.pam.task.ui.DayFragment
import lat.pam.task.ui.FridayFragment
import lat.pam.task.ui.MondayFragment
import lat.pam.task.ui.NewTodoFragment
import lat.pam.task.ui.SaturdayFragment
import lat.pam.task.ui.SundayFragment
import lat.pam.task.ui.ThursdayFragment
import lat.pam.task.ui.TodoFragment
import lat.pam.task.ui.TuesdayFragment
import lat.pam.task.ui.WednesdayFragment

class SectionPageAdapter (activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return 10
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = TodoFragment()
            1 -> fragment = DayFragment()
            2 -> fragment = MondayFragment()
            3 -> fragment = TuesdayFragment()
            4 -> fragment = WednesdayFragment()
            5 -> fragment = ThursdayFragment()
            6 -> fragment = FridayFragment()
            7 -> fragment = SaturdayFragment()
            8 -> fragment = SundayFragment()
            9 -> fragment = NewTodoFragment()
        }
        return fragment as Fragment
    }

}